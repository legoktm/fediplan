<?php
/**
 * Created by fediplan.
 * User: tom79
 */

namespace App\Form;


use App\SocialEntity\PollOption;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class PollOptionType extends AbstractType
{


    private $securityContext;

    public function __construct(Security $securityContext)
    {
        $this->securityContext = $securityContext;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', TextType::class,
            [
                'required' => false,
                'attr' => ['class' => 'form-control'],
                'label' => 'page.schedule.form.poll_item',
            ]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PollOption::class,
            'translation_domain' => 'fediplan'
        ]);
    }

}