<?php


namespace App\SocialEntity;


class PollOption
{
    /** @var string */
    private $title;
    /** @var int */
    private $votes_count;

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getVotesCount(): ?int
    {
        return $this->votes_count;
    }

    /**
     * @param int $votes_count
     */
    public function setVotesCount(?int $votes_count): void
    {
        $this->votes_count = $votes_count;
    }


    public function __toString()
    {
        return $this->title;
    }
}