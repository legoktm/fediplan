<?php

namespace App\SocialEntity;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;


class Compose
{

    private $id;

    private $content_warning;

    private $content;

    private $visibility;

    private $created_at;

    private $scheduled_at;

    private $sent_at;

    private $sensitive;

    private $in_reply_to_id;

    private $timeZone;

    private $poll_options;
    /** @var int */
    private $poll_expires_at;
    /** @var bool */
    private $poll_multiple;

    public function __construct()
    {
        $this->poll_options = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param mixed $timeZone
     */
    public function setTimeZone($timeZone): void
    {
        $this->timeZone = $timeZone;
    }

    public function getTotalMedia()
    {
    }

    public function getSent()
    {
        return ($this->sent_at != null);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContentWarning(): ?string
    {
        return $this->content_warning;
    }

    public function setContentWarning(?string $content_warning): self
    {
        $this->content_warning = $content_warning;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getVisibility(): ?string
    {
        return $this->visibility;
    }

    public function setVisibility(string $visibility): self
    {
        $this->visibility = $visibility;

        return $this;
    }


    /**
     * @return boolean
     */
    public function getSensitive()
    {
        return $this->sensitive;
    }

    /**
     * @param mixed $sensitive
     */
    public function setSensitive(bool $sensitive): void
    {
        $this->sensitive = $sensitive;
    }


    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getScheduledAt(): ?DateTime
    {
        return $this->scheduled_at;
    }

    public function setScheduledAt(?DateTime $scheduled_at): self
    {
        $this->scheduled_at = $scheduled_at;

        return $this;
    }

    public function getInReplyToId(): ?string
    {
        return $this->in_reply_to_id;
    }

    public function setInReplyToId(?string $in_reply_to_id): self
    {
        $this->in_reply_to_id = $in_reply_to_id;

        return $this;
    }

    /**
     * @return ArrayCollection|null
     */
    public function getPollOptions(): ?ArrayCollection
    {
        return $this->poll_options;
    }

    /**
     * @param ArrayCollection $poll_options
     */
    public function setPollOptions(?ArrayCollection $poll_options): void
    {
        $this->poll_options = $poll_options;
    }

    /**
     * @return int
     */
    public function getPollExpiresAt(): ?int
    {
        return $this->poll_expires_at;
    }

    /**
     * @param int $poll_expires_at
     */
    public function setPollExpiresAt(?int $poll_expires_at): void
    {
        $this->poll_expires_at = $poll_expires_at;
    }

    /**
     * @return bool
     */
    public function isPollMultiple(): ?bool
    {
        return $this->poll_multiple;
    }

    /**
     * @param bool $poll_multiple
     */
    public function setPollMultiple(?bool $poll_multiple): void
    {
        $this->poll_multiple = $poll_multiple;
    }

}
